# `marc-relators`

A crate for serializing and deserializing [MARC relators](https://www.loc.gov/marc/relators/relaterm.html).

A MARC record is a [MA-chine Readable Cataloging record](https://www.loc.gov/marc/umb/um01to06.html).
This crate does not attempt to deal with records themselves, only with their relators.

```rust
use marc_relators::MarcRelator;

let relator: MarcRelator = "aut".parse().unwrap();
assert_eq!(relator, MarcRelator::Author);

assert_eq!(relator.code(), "aut");
assert_eq!(relator.name(), "Author");
assert_eq!(
    // The full descriptions can be quite long FYI
    &relator.description().as_bytes()[0..102],
    concat!("A person, family, or organization responsible for ",
            "creating a work that is primarily textual in content").as_bytes(),
);
```

This crate tracks the most current MARC specification.
At this time, this is MARC 21.

## Anti-Copyright

Intellectual property isn't real.
There is no license.
If you insist on having one, this is [Creative Commons Zero](https://creativecommons.org/publicdomain/zero/1.0/) (public domain).
