/*
 * STOP: This file was auto-generated from a Python script.
 * Do not make edits here.
 */

use std::str::FromStr;
use crate::ParseError;

/// A MARC relator.
#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
#[non_exhaustive]
pub enum MarcRelator {
    //{% for relator in relators %}//
    /// **//{{ relator.code }}//**: //{{ relator.doc_str }}//
    //{{ relator.variant }}//,
    //{% endfor %}//
}

impl  MarcRelator {
    #[cfg(feature = "serde")]
    pub(crate) const KNOWN_VARIANTS: &'static [&'static str] = &[
        //{% for relator in relators %}//
        "//{{ relator.code }}//",
        //{% endfor %}//
    ];

    /// A three letter code used to uniquely identify the relator.
    pub const fn code(&self) -> &'static str {
        match self {
            //{% for relator in relators %}//
            Self:://{{ relator.variant }}// => "//{{ relator.code }}//",
            //{% endfor %}//
        }
    }

    /// A human-readable name of the relator.
    pub const fn name(&self) -> &'static str {
        match self {
            //{% for relator in relators %}//
            Self:://{{ relator.variant }}// => "//{{ relator.name }}//",
            //{% endfor %}//
        }
    }

    /// A (sometimes long) human-readable description of the relator.
    pub const fn description(&self) -> &'static str {
        match self {
            //{% for relator in relators %}//
            Self:://{{ relator.variant }}// => "//{{ relator.note_str }}//",
            //{% endfor %}//
        }
    }
}

impl FromStr for MarcRelator {
    type Err = ParseError;

    fn from_str(value: &str) -> Result<Self, Self::Err> {
        match value {
            //{% for relator in relators %}//
            "//{{ relator.code }}//" => Ok(Self:://{{ relator.variant }}//),
            //{% endfor %}//
            _ => Err(ParseError::UnknownCode),
        }
    }
}
